Source: nx-libs
Maintainer: Debian Remote Maintainers <debian-remote@lists.debian.org>
Uploaders: Mike Gabriel <sunweaver@debian.org>,
           Mihai Moldovan <ionic@ionic.de>,
           Antoni Villalonga <antoni@friki.cat>
Section: x11
Priority: optional
Build-Depends: autoconf,
               automake,
               debhelper-compat (= 12),
               dpkg-dev,
               expat,
               libjpeg-dev,
               libpixman-1-dev,
               libpng-dev,
               libtool,
               libxcomposite-dev,
               libxdamage-dev,
               libxdmcp-dev,
               libxext-dev,
               libxfixes-dev,
               libxfont-dev,
# libxkbfile-dev is required by xkbcomp.pc, see Debian bug #913359
               libxkbfile-dev,
               libxinerama-dev,
               libxml2-dev,
               libxpm-dev,
               libxrandr-dev,
               libxrender-dev,
               libxtst-dev,
               pkg-config,
               quilt,
               x11-xkb-utils,
               x11proto-core-dev,
               xutils-dev,
               zlib1g-dev
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/debian-remote-team/nx-libs
Vcs-Git: https://salsa.debian.org/debian-remote-team/nx-libs.git
Homepage: https://github.com/ArcticaProject/nx-libs/
Rules-Requires-Root: no

Package: nx-x11-common
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Breaks: libnx-x11 (<< 2:3.5.0.29-1~),
        nxlibs (<= 3.5.1)
Description: nx-X11 (common files)
 NX is a software suite which implements very efficient
 compression of the X11 protocol. This increases performance when
 using X applications over a network, especially a slow one.
 .
 This package provides all runtime architecture-independent files for
 nx-X11 Xserver (aka nxagent).

Package: nx-x11proto-core-dev
Architecture: any
Section: libdevel
Depends: ${misc:Depends}
Breaks: nxlibs (<= 3.5.1)
Description: nx-X11 core wire protocol and auxiliary headers
 NX is a software suite which implements very efficient
 compression of the X11 protocol. This increases performance when
 using X applications over a network, especially a slow one.
 .
 This package provides development headers describing the wire protocol
 for the core nx-X11 protocol, and also provides a number of utility headers,
 used to abstract OS-specific functions.

Package: libnx-x11-6
Architecture: any
Multi-Arch: same
Section: libs
Depends: libx11-data,
         libxcomp3 (= ${binary:Version}),
         nx-x11-common (<< ${source:Version}.1),
         nx-x11-common (>= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: libnx-x11 (<< 2:3.5.0.29-1~),
        libnx-xau6 (<< 2:3.6.0.0),
        libnx-xcomposite1 (<< 2:3.6.0.0),
        libnx-xdamage1 (<< 2:3.6.0.0),
        libnx-xdmcp6 (<< 2:3.6.0.0),
        libnx-xext6 (<< 2:3.6.0.0),
        libnx-xfixes3 (<< 2:3.6.0.0),
        libnx-xinerama1 (<< 2:3.6.0.0),
        libnx-xpm4 (<< 2:3.6.0.0),
        libnx-xrandr2 (<< 2:3.6.0.0),
        libnx-xrender1 (<< 2:3.6.0.0),
        libnx-xtst6 (<< 2:3.6.0.0),
        nxlibs (<= 3.5.1)
Replaces: libnx-xau6,
          libnx-xcomposite1,
          libnx-xdamage1,
          libnx-xdmcp6,
          libnx-xext6,
          libnx-xfixes3,
          libnx-xinerama1,
          libnx-xpm4,
          libnx-xrandr2,
          libnx-xrender1,
          libnx-xtst6,
          nxlibs
Description: nxagent's libNX_X11 client-part library
 NX is a software suite which implements very efficient
 compression of the X11 protocol. This increases performance when
 using X applications over a network, especially a slow one.
 .
 This package provides the libNX_X11 library (a libX11 drop-in
 replacement used by nxagent).

Package: libnx-x11-dev
Architecture: any
Section: libdevel
Depends: libnx-x11-6 (= ${binary:Version}),
         ${misc:Depends}
Breaks: libnx-xau-dev (<< 2:3.6.0.0),
        libnx-xcomposite-dev (<< 2:3.6.0.0),
        libnx-xdamage-dev (<< 2:3.6.0.0),
        libnx-xdmcp-dev (<< 2:3.6.0.0),
        libnx-xext-dev (<< 2:3.6.0.0),
        libnx-xfixes-dev (<< 2:3.6.0.0),
        libnx-xinerama-dev (<< 2:3.6.0.0),
        libnx-xpm-dev (<< 2:3.6.0.0),
        libnx-xrandr-dev (<< 2:3.6.0.0),
        libnx-xrender-dev (<< 2:3.6.0.0),
        libnx-xtst-dev (<< 2:3.6.0.0),
        nxlibs-dev (<=3.5.1)
Provides: libnx-x11-6-dev
Replaces: libnx-xau-dev,
          libnx-xcomposite-dev,
          libnx-xdamage-dev,
          libnx-xdmcp-dev,
          libnx-xext-dev,
          libnx-xfixes-dev,
          libnx-xinerama-dev,
          libnx-xpm-dev,
          libnx-xrandr-dev,
          libnx-xrender-dev,
          libnx-xtst-dev,
          nxlibs-dev
Description: nxagent's libNX_X11 client-part library (development headers)
 NX is a software suite which implements very efficient
 compression of the X11 protocol. This increases performance when
 using X applications over a network, especially a slow one.
 .
 This package provides development headers of the libNX_X11 library (a
 libX11 drop-in replacement used by nxagent).

Package: nx-x11proto-xext-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: ${misc:Depends}
Breaks: libnx-xext-dev (<< 2:3.5.99.0~),
        nxlibs (<= 3.5.1)
Description: nx-X11 miscellaneous extension wire protocol
 NX is a software suite which implements very efficient
 compression of the X11 protocol. This increases performance when
 using X applications over a network, especially a slow one.
 .
 This package provides development headers describing the wire protocol
 for various extensions, the client-side libraries of which are provided
 in the Xext library.

Package: nx-x11proto-composite-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: ${misc:Depends}
Breaks: nxlibs (<= 3.5.1)
Description: nx-X11 Composite extension wire protocol
 NX is a software suite which implements very efficient
 compression of the X11 protocol. This increases performance when
 using X applications over a network, especially a slow one.
 .
 This package provides development headers describing the wire protocol
 for the Composite extension in nx-X11, used to let arbitrary client
 programs control drawing of the final image.

Package: nx-x11proto-damage-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: ${misc:Depends}
Description: nx-X11 Damage extension wire protocol
 NX is a software suite which implements very efficient
 compression of the X11 protocol. This increases performance when
 using X applications over a network, especially a slow one.
 .
 This package provides development headers describing the wire protocol
 for the Damage extension, used to notify clients of changes made to
 particular areas.

Package: nx-x11proto-xfixes-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: ${misc:Depends}
Breaks: nxlibs (<= 3.5.1)
Description: nx-X11 'xfixes' extension wire protocol
 NX is a software suite which implements very efficient
 compression of the X11 protocol. This increases performance when
 using X applications over a network, especially a slow one.
 .
 This package provides development headers describing the wire protocol
 for the XFIXES extension.

Package: nx-x11proto-xinerama-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: ${misc:Depends}
Description: nx-X11 Xinerama extension wire protocol
 NX is a software suite which implements very efficient
 compression of the X11 protocol. This increases performance when
 using X applications over a network, especially a slow one.
 .
 This package provides development headers describing the wire protocol
 for the XINERAMA extension, used to use and manage a multiple-screen
 display.

Package: nx-x11proto-randr-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: ${misc:Depends}
Description: nx-X11 RandR extension wire protocol
 NX is a software suite which implements very efficient
 compression of the X11 protocol. This increases performance when
 using X applications over a network, especially a slow one.
 .
 This package provides development headers describing the wire protocol
 for the RandR extension, used to change display properties such as
 resolution, rotation, reflection, et al, on the fly.

Package: nx-x11proto-render-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: ${misc:Depends}
Description: nx-X11 Render extension wire protocol
 NX is a software suite which implements very efficient
 compression of the X11 protocol. This increases performance when
 using X applications over a network, especially a slow one.
 .
 This package provides development headers describing the wire protocol for
 the Render extension, used to implement Porter-Duff operations within X.

Package: nx-x11proto-scrnsaver-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: ${misc:Depends}
Breaks: nxlibs (<= 3.5.1)
Description: nx-X11 Screen Saver extension wire protocol
 NX is a software suite which implements very efficient
 compression of the X11 protocol. This increases performance when
 using X applications over a network, especially a slow one.
 .
 This package provides development headers describing the wire protocol
 for the MIT-SCREEN-SAVER extension, used to notify the server of client
 screen saver events.

Package: nxagent
Architecture: any
Multi-Arch: foreign
Depends: libnx-x11-6 (= ${binary:Version}),
         libxcomp3 (= ${binary:Version}),
         libxcompshad3 (= ${binary:Version}),
         x11-xkb-utils,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: xfonts-base,
            xkb-data
Breaks: libxcompext3 (<< 2:3.5.99.3~),
        nxauth
Replaces: libxcompext3,
          nxauth
Description: Nested Xserver (aka NX Agent) supporting the NX compression protocol
 NX is a software suite which implements very efficient
 compression of the X11 protocol. This increases performance when
 using X applications over a network, especially a slow one.
 .
 nxagent is a nested Xserver providing NX transport of X sessions. The
 application is based on the well known Xnest "nested" server. nxagent,
 like Xnest, is an X server for its own clients, and at the same time, an
 X client for a system's local X server.
 .
 The main scope of nxagent is to eliminate X round-trips or transform
 them into asynchronous replies. nxagent works together with nxproxy.
 nxproxy itself does not make any effort to minimize round-trips
 by itself, this is demanded of nxagent.
 .
 Being an X server, nxagent is able to resolve all the property/atoms related
 requests locally, ensuring that the most common source of round-trips are
 nearly reduced to zero.

Package: nxdialog
Architecture: all
Depends: python3,
         python3-gi,
         gir1.2-gtk-3.0,
         x11-xkb-utils,
         ${misc:Depends}
Recommends: nxagent
Breaks: nxagent (<< 2:3.5.99.21-2~)
Replaces: nxagent (<< 2:3.5.99.21-2~)
Description: Dialogs for NX Agent
 NX is a software suite which implements very efficient
 compression of the X11 protocol. This increases performance when
 using X applications over a network, especially a slow one.
 .
 nxdialog adds dialog windows/menus to nxagent. They get triggered by
 certain actions within the NX Agent Xserver.

Package: nxproxy
Architecture: any
Multi-Arch: foreign
Depends: libxcomp3 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: nxauth,
        qvd-nxproxy
Replaces: nxauth,
          qvd-nxproxy
Description: NX proxy
 NX is a software suite which implements very efficient
 compression of the X11 protocol. This increases performance when
 using X applications over a network, especially a slow one.
 .
 This package provides the NX proxy (client) binary.

Package: libxcomp3
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends},
         ${shlibs:Depends}
Breaks: nxcomp (<= 3.5.1),
        qvd-libxcomp3
Replaces: nxcomp,
          qvd-libxcomp3
Description: NX compression library
 NX is a software suite which implements very efficient
 compression of the X11 protocol. This increases performance when
 using X applications over a network, especially a slow one.
 .
 This package provides the compression library.

Package: libxcomp-dev
Architecture: any
Section: libdevel
Depends: libxcomp3 (= ${binary:Version}),
         ${misc:Depends}
Breaks: nxcomp-dev (<=3.5.1)
Provides: libxcomp3-dev
Replaces: nxcomp-dev
Description: NX compression library (development headers)
 NX is a software suite which implements very efficient
 compression of the X11 protocol. This increases performance when
 using X applications over a network, especially a slow one.
 .
 This package provides the compression library.
 .
 This package contains the development headers for this library.

Package: libxcompshad3
Architecture: any
Multi-Arch: same
Section: libs
Depends: libnx-x11-6 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: nxcompshad (<=3.5.1)
Replaces: nxcompshad
Description: NX shadowing library
 NX is a software suite which implements very efficient
 compression of the X11 protocol. This increases performance when
 using X applications over a network, especially a slow one.
 .
 This package provides a library for shadow session support.

Package: libxcompshad-dev
Architecture: any
Section: libdevel
Depends: libxcompshad3 (= ${binary:Version}),
         ${misc:Depends}
Breaks: nxcompshad-dev (<= 3.5.1)
Provides: libxcompshad3-dev
Replaces: nxcompshad-dev
Description: NX shadowing library (development headers)
 NX is a software suite which implements very efficient
 compression of the X11 protocol. This increases performance when
 using X applications over a network, especially a slow one.
 .
 This package provides a library for shadow session support.
 .
 This package contains the development headers for this library.
